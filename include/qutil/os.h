#pragma once

#if defined(__unix__)
#define OS_unix
#endif
#if defined(__linux__)
#define OS_linux
#endif

#if defined(__APPLE__)
#define OS_apple
#endif

#if defined(_WIN32)
#define OS_win32
#endif
#if defined(_WIN64)
#define OS_win64
#endif
