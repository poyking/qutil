#pragma once
#include <stdlib.h>
#include <stdint.h>
#define LIST_CMP  (1<<0) // list has cmp function
#define LIST_HASH (1<<1) // list has hash function

// basic list (order is not guarantied when inserting or removing)
struct list {
  size_t v_len;

  void  *list;         // list storing all items
  size_t list_len;     // current list length
  size_t list_max_len; // maximum list length

  uint32_t features;
  struct {
    int (*cmp)(void *v0, void *v1);
  } feat_cmp;
  struct {
    size_t (*hash)(void *v);
  } feat_hash;
};

int list_init(struct list *l, size_t v_len, size_t list_len);
int list_deinit(struct list *l);
int list_resize(struct list *l, size_t new_len);
int list_insert(struct list *l, size_t ind, void *value);

// list remove functions
int list_remove_ind(struct list *l, size_t ind);    // remove by index ()
int list_remove_hash(struct list *l, size_t hash);  // remove by hash  (LIST_HASH)
int list_remove_value(struct list *l, void *value); // remove by value (LIST_CMP)

// list get functions (returns NULL on error)
void *list_get_ind(struct list *l, size_t ind);    // get by index ()
void *list_get_hash(struct list *l, size_t hash);  // get by hash  (LIST_HASH)
void *list_get_value(struct list *l, void *value); // get by value (LIST_CMP)

// list stack functions
int list_push(struct list *l, void *value); // push value (read from value)
int list_pop(struct list *l,  void *value); // pop into value (write to value)

// utility
struct list list_copy(struct list *l);

#define lst_init(_l, _vt, _len) list_init(_l, sizeof(_vt), _len)
#define lst_deinit(_l) list_deinit(_l)
#define lst_resize(_l, _len) list_resize(_l, _len)
#define lst_insert(_l, _vt, _ind, _v) ({ _vt v = _v; list_insert(_l, _ind, &v); })

#define lst_remove_ind(_l, _ind) list_remove_ind(_l, _ind)
#define lst_remove_hash(_l, _hash) list_remove_hash(_l, _hash)
#define lst_remove_value(_l, _vt, _v) ({ _vt v = _v; list_remove_value(_l, &v); })

#define lst_get_ind(_l, _vt, _ind) ((_vt *)list_get_ind(_l, _ind))
#define lst_get_hash(_l, _vt, _hash) ((_vt *)list_get_hash(_l, _hash))
#define lst_get_value(_l, _vt, _v) ({ _vt v = _v; (_vt *)list_get_value(_l, &v); })

#define lst_push(_l, _vt, _v) ({ _vt v = _v; list_push(_l, &v); })
#define lst_pop(_l, _v) list_pop(_l, _v)

#define lst_copy(_l) list_copy(_l)

// utility
#define lst_len(_l)       ((_l)->list_len)
#define lst_max_len(_l)   ((_l)->list_max_len)
#define lst_data(_l, _vt) ((_vt *)(_l)->list)
#define lst_iter(_l, _vt, _i, _expr) ({                                 \
      int retval = 0;                                                   \
      for (size_t __i = 0; __i < lst_len(_l); __i++) {                  \
        _vt *_i = NULL;                                                 \
        if ((_i = lst_get_ind(_l, _vt, __i)) == NULL) { retval = -1; break; } \
        {_expr;}                                                        \
      }                                                                 \
      retval;                                                           \
    })
