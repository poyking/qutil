#pragma once
#include <qutil/util.h>
#include <stdlib.h>
#include <stdint.h>

struct hashtable {
  size_t k_len;
  size_t v_len;

  void  *buckets;
  size_t num_buckets;
  size_t used_buckets;

  size_t (*hasher)(void *key);
  int    (*key_cmp)(void *k0, void *k1);
};

int hashtable_init(struct hashtable *ht, size_t k_len, size_t v_len, size_t num_buckets, size_t (*hasher)(void *key), int (*key_cmp)(void *k0, void *k1));
int hashtable_deinit(struct hashtable *ht);
int hashtable_resize(struct hashtable *ht, size_t new_size); // new size must be larger that current size
int hashtable_insert(struct hashtable *ht, void *key, void *value);
int hashtable_remove(struct hashtable *ht, void *key);
void *hashtable_get(struct hashtable *ht, void *key); // returns NULL on error

#define ht_init(_ht, _kt, _vt, _nb, _hasher, _key_cmp) hashtable_init(_ht, sizeof(_kt), sizeof(_vt), _nb, (size_t (*)(void *))_hasher, (int (*)(void *, void *))_key_cmp)
#define ht_deinit(_ht) hashtable_deinit(_ht)
#define ht_resize(_ht, _ns) hashtable_resize(_ht, _ns)
#define ht_insert(_ht, _kt, _vt, _k, _v) ({ _kt k = _k; _vt v = _v; hashtable_insert(_ht, &k, &v); })
#define ht_remove(_ht, _kt, _k) ({ _kt k = _k; hashtable_remove(_ht, &k); })
#define ht_get(_ht, _kt, _vt, _k) ({ _kt k = _k; (_vt *)hashtable_get(_ht, &k); })
#define ht_len(_ht) ((_ht)->used_buckets)
#define ht_iter(_ht, _kt, _vt, _ki, _vi, _expr) ({                      \
      int retval = 0;                                                   \
      for (size_t __i = 0; __i < (_ht)->num_buckets; __i++) {           \
        _kt *_ki = NULL;                                                \
        _vt *_vi = NULL;                                                \
        void *bucket = (_ht)->buckets+((sizeof(uint8_t)+(_ht)->k_len+(_ht)->v_len)*__i); \
        if (*(uint8_t *)bucket) {                                       \
          _ki = (_kt *)(bucket+sizeof(uint8_t));                        \
          _vi = (_vt *)(bucket+sizeof(uint8_t)+(_ht)->k_len);           \
          { _expr; }                                                    \
        }                                                               \
      }                                                                 \
      retval;                                                           \
    })
