#pragma once
#include <qutil/refcount.h>
#include <qutil/hashtable.h>
#include <qutil/list.h>

struct json_object;
struct json_array;
struct json_value {
  enum {
    JSON_STRING,
    JSON_NUMBER,
    JSON_OBJECT,
    JSON_ARRAY,
    JSON_BOOL,
    JSON_NULL,
  } type;
  union {
    char               *string; // RC
    double              number;
    struct json_object *object;
    struct json_array  *array;
    int                 bool;
    struct {}           null;
  };
};
struct json_object {
  struct hashtable values; // char */*RC*/, struct json_value
};
struct json_array {
  struct list values; // struct json_value
};
struct json {
  struct list objects; // struct json_object

  void *data;
  char (*next)(void *);
  char la; // look ahead
};

int json_init(struct json *j, char (*next_ch)(void *), void *data);
int json_deinit(struct json *j);

int json_parse_object(struct json *j, struct json_object *obj);
int json_parse_array(struct json *j, struct json_array *arr);
int json_parse_value(struct json *j, struct json_value *val);
int json_parse(struct json *j);
