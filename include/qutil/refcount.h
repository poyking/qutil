#pragma once
#include <stdlib.h>
#include <stdatomic.h>

#ifdef __cplusplus
#include <atomic>
using std::atomic_size_t;
#endif

/* when using refcounted objects make
 * sure to annotate there definitions
 * in structures to avoid confusion.
 * annotate their definitions with:
 * // RC
 * or in the case of a function add a comment
 * within the argument list and document the function
 * argument as being refcounted */

struct rc_header {
  size_t        len; // length of the allocation
  atomic_size_t ref; // number of references
  
  char data[]; // allocated data
};

// allocate a new refcounted object
void *rc_alloc(size_t len);
// free a reference to a refcounted object
void rc_free(void *);

// make a reference/copy of a refcounted object
void *rc_ref(void *);

// UTILITY
char *rc_string(const char *);
