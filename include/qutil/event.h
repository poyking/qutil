/* all functions returning int return -1 on error and 0 on success
 * errors are put in the global errno value */
#pragma once
#include <errno.h>
#include <stdlib.h>
#include <qutil/os.h>
#include <qutil/refcount.h>

#if defined(OS_linux)
#include <sys/epoll.h>
typedef uint32_t event_poll_t;
#define EVPOLL_IN  EPOLLIN  // fd ready for read operations
#define EVPOLL_OUT EPOLLOUT // fd ready for write operations
#define EVPOLL_ERR EPOLLERR // error on fd
#define EVPOLL_HUP EPOLLHUP // connection interrupted
#elif defined(OS_unix)
#include <sys/poll.h>
typedef short event_poll_t;
#define EVPOLL_IN  POLLIN  // fd ready for read operations
#define EVPOLL_OUT POLLOUT // fd ready for write operations
#define EVPOLL_ERR POLLERR // error on fd
#define EVPOLL_HUP POLLHUP // error on fd
#error "TODO: implment event with non linux specific interfaces"
#endif

// poll for data on a file descriptor
struct event_poll {
#if defined(OS_linux)
  int epoll_fd;
#elif defined (OS_unix)
#endif
};
struct event_poll_event {
  void        *data;
  event_poll_t events; // triggered events
};

int event_poll_init(struct event_poll *p);
int event_poll_deinit(struct event_poll *p);
// add or modify an entry
int event_poll_add(struct event_poll *p, int fd, void *data, event_poll_t events);
// modify an entry
int event_poll_mod(struct event_poll *p, int fd, void *data, event_poll_t events);
// remove an entry
int event_poll_rem(struct event_poll *p, int fd);
// wait for an event
int event_poll_wait(struct event_poll *p, struct event_poll_event *ev, int timeout);

#if defined(OS_unix)
typedef uint32_t event_watch_t;
#define EVWATCH_ACCESS (1<<0)
#endif

struct event_watch_path {
#if defined(OS_unix)
  /* modication time at last check
   * if this changes it means the file was modified */
  struct timespec mtime;
#endif

  // path of the file
  char *path; // RC
  // events to watch for
  event_watch_t events;

  // extra data
  void *data;

  struct event_watch_path *next;
};
struct event_watch {
  struct event_watch_path *watches;
};

int event_watch_init(struct event_watch *w);
int event_watch_deinit(struct event_watch *w);
// add or modify an entry
int event_watch_add(struct event_watch *w, const char *path, void *data, event_watch_t mask);
// remove an entry
int event_watch_rem(struct event_watch *w, const char *path, void **data);
/* wait for an event
 * interval: how often to check for a change */
int event_watch_wait(struct event_watch *w, const char **path, void **data, event_watch_t *mask, int interval, int timeout);
