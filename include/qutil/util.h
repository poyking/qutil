#pragma once
#include <stdlib.h>
#include <stdint.h>

// hash and cmp functions
#define __SIMPLE_CMP_HASH(_type, _name)                                 \
  static __attribute__((unused)) size_t ht_##_name##_hash(_type *v)            { return *v; } \
  static __attribute__((unused)) int ht_##_name##_cmp(_type *v0, _type *v1) { return *v0 == *v1; }
size_t ht_str_hash(char **s);
int ht_str_cmp(char **s0, char **s1);
__SIMPLE_CMP_HASH(char, char);
__SIMPLE_CMP_HASH(short, sint);
__SIMPLE_CMP_HASH(int, int);
__SIMPLE_CMP_HASH(long, lint);
__SIMPLE_CMP_HASH(unsigned char, uchar);
__SIMPLE_CMP_HASH(unsigned short, suint);
__SIMPLE_CMP_HASH(unsigned int, uint);
__SIMPLE_CMP_HASH(unsigned long, luint);

__SIMPLE_CMP_HASH(int8_t,   int8);
__SIMPLE_CMP_HASH(int16_t,  int16);
__SIMPLE_CMP_HASH(int32_t,  int32);
__SIMPLE_CMP_HASH(int64_t,  int64);
__SIMPLE_CMP_HASH(uint8_t,  uint8);
__SIMPLE_CMP_HASH(uint16_t, uint16);
__SIMPLE_CMP_HASH(uint32_t, uint32);
__SIMPLE_CMP_HASH(uint64_t, uint64);
#undef __SIMPLE_CMP_HASH
