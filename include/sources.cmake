file(GLOB_RECURSE QUTIL_TMP_HEADERS
  ${PROJECT_SOURCE_DIR}/include/*.h
  ${PROJECT_SOURCE_DIR}/include/*.hpp)

qutil_add_headers(${QUTIL_TMP_HEADERS})
