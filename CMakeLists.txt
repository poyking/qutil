cmake_minimum_required(VERSION 3.22)
# version
set(QUTIL_VERSION_MAJOR 2)
set(QUTIL_VERSION_MINOR 6)
set(QUTIL_VERSION_PATCH 1)
set(QUTIL_VERSION       ${QUTIL_VERSION_MAJOR}.${QUTIL_VERSION_MINOR}.${QUTIL_VERSION_PATCH})

# generic definitions
set(QUTIL_LIBRARY qutil)
set(QUTIL         ${QUTIL_LIBRARY})
set(QUTIL_SOURCES "")
set(QUTIL_HEADERS "")

# functions
macro(qutil_add_sources)
  list(APPEND QUTIL_SOURCES ${ARGN})
endmacro()
macro(qutil_add_headers)
  qutil_add_sources(${ARGN})
  list(APPEND QUTIL_HEADERS ${ARGN})
endmacro()

project(${QUTIL} CXX C)
include(GNUInstallDirs)

include(src/sources.cmake)
include(include/sources.cmake)

add_library(
  ${QUTIL}
  SHARED
  ${QUTIL_SOURCES}
  )
set_target_properties(${QUTIL} PROPERTIES VERSION   ${QUTIL_VERSION})
set_target_properties(${QUTIL} PROPERTIES VERSION   ${QUTIL_VERSION})
set_target_properties(${QUTIL} PROPERTIES SOVERSION ${QUTIL_VERSION_MAJOR})

# generate files from templates
configure_file(
  ${CMAKE_CURRENT_SOURCE_DIR}/${QUTIL_LIBRARY}.pc.in
  ${CMAKE_CURRENT_BINARY_DIR}/${QUTIL_LIBRARY}-${QUTIL_VERSION_MAJOR}.pc
  @ONLY
  ) 

install(TARGETS ${QUTIL})
install(FILES   ${QUTIL_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${QUTIL_LIBRARY}-${QUTIL_VERSION_MAJOR}.pc
  DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/pkgconfig)

# uninstall target
add_custom_target("uninstall" COMMENT "uninstall installed files")
add_custom_command(
  TARGET "uninstall"
  POST_BUILD
  COMMENT "uninstall files with install_manifest.txt"
  COMMAND cat install_manifest.txt 2>/dev/null | xargs rm -vf || echo "no installed files" &&
          rm -f install_manifest.txt || echo "no installed files"
  )
