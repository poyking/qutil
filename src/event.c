#include <qutil/event.h>

#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>

int event_poll_init(struct event_poll *p) {
#if defined(OS_linux)
  if ((p->epoll_fd = epoll_create(1024)) < 0) {
    return -1;
  }
#elif defined(OS_unix)
#endif

  return 0;
}
int event_poll_deinit(struct event_poll *p) {
#if defined(OS_linux)
  if (close(p->epoll_fd) < 0) {
    switch (errno) {
    case EBADF: errno = EINVAL; return -1;
    default:                    return -1;
    }
  }
#elif defined(OS_unix)
#endif

  return 0;
}
int event_poll_add(struct event_poll *p, int fd, void *data, event_poll_t events) {
#if defined(OS_linux)
  struct epoll_event ev = {
    .events = events,
    .data = {
      .ptr = data,
    },
  };
  if (epoll_ctl(p->epoll_fd, EPOLL_CTL_ADD, fd, &ev) < 0) {
  err_handler:
    switch (errno) {
    case EPERM:
    case EBADF: errno = EINVAL; return -1;
    case EEXIST: {
      if (epoll_ctl(p->epoll_fd, EPOLL_CTL_MOD, fd, &ev) < 0) {
        goto err_handler;
      }
    } break;
    default: return -1;
    }
  }
#elif defined(OS_unix)
#endif

  return 0;
}
int event_poll_mod(struct event_poll *p, int fd, void *data, event_poll_t events) {
#if defined(OS_linux)
  struct epoll_event ev = {
    .events = events,
    .data = {
      .ptr = data,
    },
  };

  if (epoll_ctl(p->epoll_fd, EPOLL_CTL_MOD, fd, &ev) < 0) {
    switch (errno) {
    case EPERM:
    case EBADF: errno = EINVAL; return -1;
    default: return -1;
    }
  }
#elif defined(OS_unix)
#endif

  return 0;
}
int event_poll_rem(struct event_poll *p, int fd) {
#if defined(OS_linux)
  if (epoll_ctl(p->epoll_fd, EPOLL_CTL_DEL, fd, NULL) < 0) {
    switch (errno) {
    case EPERM:
    case EBADF: errno = EINVAL; return -1;
    default: return -1;
    }
  }
#elif defined(OS_unix)
#endif

  return 0;
}
int event_poll_wait(struct event_poll *p, struct event_poll_event *ev, int timeout) {
  if (!ev) {
    errno = EINVAL;
    return -1;
  }
  int _res = 0;
#if defined(OS_linux)
  struct epoll_event _ev;
  if ((_res = epoll_wait(p->epoll_fd, &_ev, 1, timeout)) < 0) {
    switch (errno) {
    case EINVAL:
    case EFAULT:
    case EBADF: errno = EINVAL; return -1;
    default: return -1;
    }
  }
  ev->data   = _ev.data.ptr;
  ev->events = _ev.events;
  return _res;
#elif defined(OS_unix)
  return 0;
#endif
}

int event_watch_init(struct event_watch *w) {
  w->watches = NULL;

  return 0;
}
int event_watch_deinit(struct event_watch *w) {
  struct event_watch_path this = {};
  for (struct event_watch_path *p = w->watches; p; p = this.next) {
    this = *p;

    rc_free(p->path);
    free(p);
  }

  return 0;
}
int event_watch_add(struct event_watch *w, const char *path, void *data, event_watch_t mask) {
  struct event_watch_path *p = malloc(sizeof(struct event_watch_path));
#if defined(OS_unix)
  // get the modification time of this file
  struct stat st;
  if (stat(path, &st) < 0) {
    free(p);
    return -1;
  }
  p->mtime = st.st_mtim;
#endif
  p->path   = rc_string(path);
  p->events = mask;
  p->data   = data;
  p->next   = w->watches;

  w->watches = p;

  return 0;
}
int event_watch_rem(struct event_watch *w, const char *path, void **data) {
  struct event_watch_path *prev = NULL;
  for (struct event_watch_path *p = w->watches; p; p = p->next) {
    if (strcmp(p->path, path) == 0) {
      if (prev) {
        prev->next = p->next;
      }
      else {
        w->watches = p->next;
      }
      if (data) *data = p->data;
      rc_free(p->path);
      free(p);
      return 0;
    }
    prev = p;
  }

  errno = ENOENT;
  return -1;
}
int event_watch_wait(struct event_watch *w, const char **path, void **data, event_watch_t *mask, int interval, int timeout) {
#if defined(OS_unix)
  int time = 0;
#endif

  if (timeout == 0) {
    *mask = 0;
    *path = NULL;
    return 0;
  }

#if defined(OS_unix)
  struct timespec start;
  if (clock_gettime(CLOCK_MONOTONIC, &start) < 0) {
    errno = ENOSYS;
    return -1;
  }
  while (time < (timeout < 0 ? 0 : timeout) || timeout < 0) {
    struct event_watch_path *prev = NULL;
    for (struct event_watch_path *p = w->watches; p;) {
      struct stat st;
      if (stat(p->path, &st) < 0) {
        switch (errno) {
        default: {
          struct event_watch_path *pa = p;
          if (prev) prev->next = p->next;
          else      w->watches = p->next;
          p = p->next;
          free(pa);
          continue;
        } break;
        }
      }
      prev = p;

      int           modified = 0;
      event_watch_t mmask    = 0;
      if (p->events & EVWATCH_ACCESS) {
        if (st.st_mtim.tv_sec == p->mtime.tv_sec &&
            st.st_mtim.tv_nsec != p->mtime.tv_nsec) {
          modified = 1;
          mmask   |= EVWATCH_ACCESS;
        }
        else if (st.st_mtim.tv_sec != p->mtime.tv_sec) {
          modified = 1;
          mmask   |= EVWATCH_ACCESS;
        }
        p->mtime = st.st_mtim;
      }

      if (modified) {
        if (mask) *mask = mmask;
        if (path) *path = p->path;
        if (data) *data = p->data;
        return 0;
      }
      p = p->next;
    }

    struct timespec now;
    if (clock_gettime(CLOCK_MONOTONIC, &now) < 0) {
      errno = ENOSYS;
      return -1;
    }
    time = (now.tv_sec*1000+now.tv_nsec/1000000)-(start.tv_sec*1000+start.tv_nsec/1000000);
    if (interval > 0) {
      if (usleep(interval) < 0) {
        return -1;
      }
    }
  }
#endif

  return 0;
}
