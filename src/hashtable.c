#include <qutil/hashtable.h>

#include <errno.h>
#include <stdint.h>
#include <string.h>
//                        used            key          value
#define ELEMENT_LEN(_ht) (sizeof(uint8_t)+(_ht)->k_len+(_ht)->v_len)

int hashtable_init(struct hashtable *ht, size_t k_len, size_t v_len, size_t num_buckets, size_t (*hasher)(void *key), int (*key_cmp)(void *k0, void *k1)) {
  if (!ht || k_len == 0 || v_len == 0 || num_buckets == 0) {
    errno = EINVAL;
    return -1;
  }
  ht->k_len        = k_len;
  ht->v_len        = v_len;
  ht->num_buckets  = num_buckets;
  ht->used_buckets = 0;
  ht->buckets      = malloc(ELEMENT_LEN(ht)*ht->num_buckets);
  ht->hasher       = hasher;
  ht->key_cmp      = key_cmp;

  memset(ht->buckets, 0, ELEMENT_LEN(ht)*ht->num_buckets);

  return 0;
}
int hashtable_deinit(struct hashtable *ht) {
  if (!ht || ht->k_len == 0 || ht->v_len == 0 || ht->num_buckets == 0) {
    errno = EINVAL;
    return -1;
  }

  ht->k_len        = 0;
  ht->v_len        = 0;
  ht->num_buckets  = 0;
  ht->used_buckets = 0;
  free(ht->buckets);

  return 0;
}
int hashtable_resize(struct hashtable *ht, size_t new_size) {
  if (!ht || ht->k_len == 0 || ht->v_len == 0 || ht->num_buckets == 0 || new_size < ht->num_buckets) {
    errno = EINVAL;
    return -1;
  }

  struct hashtable n_ht;
  hashtable_init(&n_ht, ht->k_len, ht->v_len, new_size, ht->hasher, ht->key_cmp);
  for (size_t i = 0; i < ht->num_buckets; i++) {
    size_t ind = i;

    void    *bptr  = ht->buckets+(ELEMENT_LEN(ht)*ind);
    uint8_t *used  = bptr;
    void    *key   = bptr+1;
    void    *value = bptr+1+ht->k_len;

    if ((*used)) { // insert key value pair into new hash table
      hashtable_insert(&n_ht, key, value);
    }
  }
  hashtable_deinit(ht);
  *ht = n_ht;

  return 0;
}
int hashtable_insert(struct hashtable *ht, void *_key, void *_value) {
  if (!ht || !_key || !_value || ht->k_len == 0 || ht->v_len == 0 || ht->num_buckets == 0) {
    errno = EINVAL;
    return -1;
  }

  size_t hash = ht->hasher(_key);
  for (size_t i = 0; i < ht->num_buckets; i++) {
    size_t ind = (hash+i)%ht->num_buckets;

    void    *bptr  = ht->buckets+(ELEMENT_LEN(ht)*ind);
    uint8_t *used  = bptr;
    void    *key   = bptr+1;
    void    *value = bptr+1+ht->k_len;

    if ((*used)) {
      if (ht->key_cmp(key, _key)) {
        errno = EEXIST;
        return -1;
      }
    }
    if (!(*used)) {
      *used = 1;
      memcpy(key,   _key,   ht->k_len);
      memcpy(value, _value, ht->v_len);
      ht->used_buckets++;

      if (ht->used_buckets > ht->num_buckets/2) { // double hash table size
        if (hashtable_resize(ht, ht->num_buckets*2) < 0) {
          return -1;
        }
      }

      break;
    }
  }

  return 0;
}
int hashtable_remove(struct hashtable *ht, void *_key) {
  if (!ht || !_key || ht->k_len == 0 || ht->v_len == 0 || ht->num_buckets == 0) {
    errno = EINVAL;
    return -1;
  }

  size_t hash = ht->hasher(_key);
  for (size_t i = 0; i < ht->num_buckets; i++) {
    size_t ind = (hash+i)%ht->num_buckets;

    void    *bptr  = ht->buckets+(ELEMENT_LEN(ht)*ind);
    uint8_t *used  = bptr;
    void    *key   = bptr+1;

    if ((*used)) {
      if (ht->key_cmp(key, _key)) {
        ht->used_buckets--;

        *used = 0;
        return 0;
      }
    }
  }

  errno = ENOENT;
  return -1;
}
void *hashtable_get(struct hashtable *ht, void *_key) {
  if (!ht || !_key || ht->k_len == 0 || ht->v_len == 0 || ht->num_buckets == 0) {
    errno = EINVAL;
    return NULL;
  }

  size_t hash = ht->hasher(_key);
  for (size_t i = 0; i < ht->num_buckets; i++) {
    size_t ind = (hash+i)%ht->num_buckets;

    void    *bptr  = ht->buckets+(ELEMENT_LEN(ht)*ind);
    uint8_t *used  = bptr;
    void    *key   = bptr+1;
    void    *value = bptr+1+ht->k_len;

    if ((*used)) {
      if (ht->key_cmp(key, _key)) {
        return value;
      }
    }
  }

  errno = ENOENT;
  return NULL;
}
