#include <qutil/util.h>

#include <string.h>

// hash and cmp functions
size_t ht_str_hash(char **s) {
  size_t hash = 0;
  for (int i = 0; i < strlen(*s); i++) {
    hash += (*s)[i]*i;
  }

  return hash;
}
int ht_str_cmp(char **s0, char **s1) {
  return strcmp(*s0, *s1) == 0;
}
