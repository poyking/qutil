#include <qutil/refcount.h>

#include <stddef.h>
#include <string.h>
#include <errno.h>

void *rc_alloc(size_t len) {
  if (len <= 0) {
    errno = EINVAL;
    return NULL;
  }

  struct rc_header *ptr = malloc(sizeof(struct rc_header)+len);
  if (!ptr) {
    return NULL;
  }
  memset(ptr, 0, sizeof(struct rc_header)+len);

  ptr->len = len;
  ptr->ref = 1;

  return ptr->data;
}
void rc_free(void *ptr) {
  struct rc_header *head = ptr-offsetof(struct rc_header, data);

  if (--head->ref <= 0) {
    free(head);
  }
}
void *rc_ref(void *ptr) {
  struct rc_header *head = ptr-offsetof(struct rc_header, data);
  head->ref++;

  return ptr;
}

char *rc_string(const char *s) {
  char *str = rc_alloc(strlen(s)+1);
  if (!str) {
    return NULL;
  }
  strcpy(str, s);

  return str;
}
