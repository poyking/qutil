#include <qutil/list.h>

#include <string.h>
#include <errno.h>

int list_init(struct list *l, size_t v_len, size_t list_len) {
  if (!l || v_len <= 0 || list_len <= 0) {
    errno = EINVAL;
    return -1;
  }

  l->list         = NULL;
  l->v_len        = v_len;
  l->list_len     = 0;
  l->list_max_len = 0;
  l->features     = 0;

  if (list_resize(l, list_len) < 0) {
    return -1;
  }

  return 0;
}
int list_deinit(struct list *l) {
  if (!l || !l->list) {
    errno = EINVAL;
    return -1;
  }

  free(l->list);

  return 0;
}
int list_resize(struct list *l, size_t new_len) {
  if (!l || new_len < l->list_len) {
    errno = EINVAL;
    return -1;
  }

  l->list_max_len = new_len;

  size_t list_len_bytes = l->list_max_len*l->v_len;
  if (!l->list) l->list = malloc(list_len_bytes);
  else {
    l->list = realloc(l->list, list_len_bytes);
  }
  if (!l->list) {
    return -1;
  }

  return 0;
}
int list_insert(struct list *l, size_t ind, void *value) {
  if (!l || !value || l->list_len >= ind) {
    errno = EINVAL;
    return -1;
  }

  if (list_push(l, l->list+(ind*l->v_len)) < 0) {
    return -1;
  }
  memmove(l->list+(ind*l->v_len), value, l->v_len);

  return 0;
}
int list_remove_ind(struct list *l, size_t ind) {
  if (!l || ind >= l->list_len) {
    errno = EINVAL;
    return -1;
  }

  if (list_pop(l, l->list+(ind*l->v_len)) < 0) {
    return -1;
  }

  return 0;
}
int list_remove_hash(struct list *l, size_t hash) {
  if (!l || (l->features&LIST_HASH) <= 0) {
    errno = EINVAL;
    return -1;
  }

  size_t ind   = 0;
  int    found = 0;
  for (size_t i = 0; i < l->list_len; i++) {
    if (l->feat_hash.hash(l->list+(i*l->v_len)) == hash) {
      ind   = i;
      found = 1;
      break;
    }
  }
  if (!found) {
    errno = ENOENT;
    return -1;
  }

  if (list_remove_ind(l, ind) < 0) {
    return -1;
  }

  return 0;
}
int list_remove_value(struct list *l, void *value) {
  if (!l || (l->features&LIST_CMP) <= 0) {
    errno = EINVAL;
    return -1;
  }

  size_t ind   = 0;
  int    found = 0;
  for (size_t i = 0; i < l->list_len; i++) {
    if (l->feat_cmp.cmp(l->list+(i*l->v_len), value)) {
      ind   = i;
      found = 1;
      break;
    }
  }
  if (!found) {
    errno = ENOENT;
    return -1;
  }

  if (list_remove_ind(l, ind) < 0) {
    return -1;
  }

  return 0;
}
void *list_get_ind(struct list *l, size_t ind) {
  if (!l || ind >= l->list_len) {
    errno = EINVAL;
    return NULL;
  }

  return l->list+(ind*l->v_len);
}
void *list_get_hash(struct list *l, size_t hash) {
  if (!l || (l->features&LIST_HASH) <= 0) {
    errno = EINVAL;
    return NULL;
  }

  size_t ind   = 0;
  int    found = 0;
  for (size_t i = 0; i < l->list_len; i++) {
    if (l->feat_hash.hash(l->list+(i*l->v_len)) == hash) {
      ind   = i;
      found = 1;
      break;
    }
  }
  if (!found) {
    errno = ENOENT;
    return NULL;
  }

  return l->list+(ind*l->v_len);
}
void *list_get_value(struct list *l, void *value) {
  if (!l || (l->features&LIST_CMP) <= 0) {
    errno = EINVAL;
    return NULL;
  }

  size_t ind   = 0;
  int    found = 0;
  for (size_t i = 0; i < l->list_len; i++) {
    if (l->feat_cmp.cmp(l->list+(i*l->v_len), value)) {
      ind   = i;
      found = 1;
      break;
    }
  }
  if (!found) {
    errno = ENOENT;
    return NULL;
  }

  return l->list+(ind*l->v_len);
}
int list_push(struct list *l, void *value) {
  if (!l || !value) {
    errno = EINVAL;
    return -1;
  }

  if (l->list_max_len < l->list_len+1) {
    if (list_resize(l, l->list_max_len*2) < 0) {
      return -1;
    }
  }
  memmove(l->list+((l->list_len++)*l->v_len), value, l->v_len);

  return 0;
}
int list_pop(struct list *l, void *value) {
  if (!l || !value) {
    errno = EINVAL;
    return -1;
  }
  memmove(value, l->list+((--l->list_len)*l->v_len), l->v_len);

  return 0;
}
struct list list_copy(struct list *l) {
  struct list lst = *l;
  lst.list        = malloc(lst.list_max_len*lst.v_len);
  memcpy(lst.list, l->list, lst.list_max_len*lst.v_len);

  return lst;
}
