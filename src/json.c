#include <qutil/json.h>

#include <string.h>
#include <errno.h>

char json_next_ch(struct json *j) {
  if (j->la < 0) {
    char ch = j->next(j->data);
    return ch;
  }
  else {
    char ch = j->la;
    j->la   = -1;
    return ch;
  }
}
char json_peek_ch(struct json *j) {
  if (j->la < 0) {
    j->la = json_next_ch(j);
  }

  return j->la;
}

int json_init(struct json *j, char (*next_ch)(void *), void *data) {
  if (!j) {
    errno = EINVAL;
    return -1;
  }

  j->next = next_ch;
  j->data = data;

  return 0;
}
int json_deinit(struct json *j) {
  return 0;
}

// ---- JSON PARSER ----
int json_parse_whitespace(struct json *j) {
  while (1) {
    char ch = json_peek_ch(j);
    if (ch == ' ')  {json_next_ch(j);continue;}
    if (ch == '\n') {json_next_ch(j);continue;}
    if (ch == '\r') {json_next_ch(j);continue;}
    if (ch == '\t') {json_next_ch(j);continue;}
    return 0;
  }

  errno = ENOSYS;
  return -1;
}
char */*RC*/json_parse_string(struct json *j) {
  struct list string; // char
  if (lst_init(&string, char, 1024) < 0) return NULL;

  if (json_peek_ch(j) != '"') return NULL;
  json_next_ch(j);
  while (1) {
    if (json_peek_ch(j) == '"') break;

    char ch = json_next_ch(j);
    if (ch == '\\') {
      switch (json_peek_ch(j)) {
      case '"':  ch = '"';  json_next_ch(j); break;
      case '\\': ch = '\\'; json_next_ch(j); break;
      case '/':  ch = '/';  json_next_ch(j); break;
      case 'b':  ch = '\b'; json_next_ch(j); break;
      case 'f':  ch = '\f'; json_next_ch(j); break;
      case 'n':  ch = '\n'; json_next_ch(j); break;
      case 'r':  ch = '\r'; json_next_ch(j); break;
      case 't':  ch = '\t'; json_next_ch(j); break;
      case 'u': {
        ch = json_next_ch(j);
        if (lst_push(&string, char, ch) < 0) return NULL;
        ch = json_next_ch(j);
        if (lst_push(&string, char, ch) < 0) return NULL;
        ch = json_next_ch(j);
        if (lst_push(&string, char, ch) < 0) return NULL;
        ch = json_next_ch(j);
      } break;
      default:
        return NULL;
      }
    }

    if (lst_push(&string, char, ch) < 0) return NULL;
  }
  if (json_peek_ch(j) != '"') return NULL;
  json_next_ch(j);

  // put var string into a RC string
  if (lst_push(&string, char, '\0') < 0) return NULL;
  char *str = rc_alloc(lst_len(&string));
  memcpy(str, lst_data(&string, char), lst_len(&string));

  // clean
  if (lst_deinit(&string) < 0) return NULL;

  return str;
}
int json_is_number(char ch) {
  switch (ch) {
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    return 1;
  default:
    return 0;
  }
}
int json_parse_number(struct json *j, double *num) {
  struct list number; // char
  // STATE
  struct {
    int dot_used;
  } state = {
    .dot_used = 0,
  };
  // END STATE
  if (lst_init(&number, char, 1024) < 0) return -1;

  if (json_peek_ch(j) == '-') {
    if (lst_push(&number, char, json_next_ch(j)) < 0) return -1; 
  }
  else if (json_is_number(json_peek_ch(j))) {
    if (lst_push(&number, char, json_next_ch(j)) < 0) return -1; 
  }

  while (1) {
    if (json_is_number(json_peek_ch(j))) {
      if (lst_push(&number, char, json_next_ch(j)) < 0) return -1; 
    }
    else if (json_peek_ch(j) == '.' && !state.dot_used) {
      if (lst_push(&number, char, json_next_ch(j)) < 0) return -1; 
      state.dot_used = 1;
    }
    else {
      break;
    }
  }
  if (lst_push(&number, char, '\0') < 0) return -1; 

  // convert
  *num = strtod(lst_data(&number, char), NULL);

  // clean
  lst_deinit(&number);

  return 0;
}
int json_parse_bool(struct json *j, int *bool) {
  if (json_peek_ch(j) == 't') { // true
    *bool = 1;

    json_next_ch(j);
    if (json_peek_ch(j) != 'r') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'u') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'e') return -1;
    json_next_ch(j);
  }
  else if (json_peek_ch(j) == 'f') { // false
    *bool = 0;

    json_next_ch(j);
    if (json_peek_ch(j) != 'a') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'l') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 's') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'e') return -1;
    json_next_ch(j);
  }

  return 0;
}
int json_parse_object(struct json *j, struct json_object *obj) {
  if (ht_init(&obj->values,
              char *, struct json_value, 16,
              ht_str_hash, ht_str_cmp) < 0) {
  }

  if (json_peek_ch(j) != '{') return -1;
  json_next_ch(j);
  if (json_parse_whitespace(j) < 0) return -1;
  if (json_peek_ch(j) != '}') { // parse list
    while (1) {
      if (json_parse_whitespace(j) < 0) return -1;
      char *key = json_parse_string(j); // RC
      if (!key) return -1;
      if (json_parse_whitespace(j) < 0) return -1;
      if (json_peek_ch(j) != ':') return -1;
      json_next_ch(j);
      struct json_value val;
      if (json_parse_value(j, &val) < 0) return -1;
      if (ht_insert(&obj->values, char *, struct json_value, key, val) < 0) return -1;
      if (json_peek_ch(j) != ',') break;
      json_next_ch(j);
    }
    if (json_peek_ch(j) != '}') return -1;
  }
  json_next_ch(j);

  return 0;
}
int json_parse_array(struct json *j, struct json_array *arr) {
  if (lst_init(&arr->values, struct json_value, 16) < 0) return -1;

  if (json_peek_ch(j) != '[') return -1;
  json_next_ch(j);
  if (json_parse_whitespace(j) < 0) return -1;
  if (json_peek_ch(j) != ']') { // parse list
    while (1) {
      if (json_parse_whitespace(j) < 0) return -1;
      struct json_value val;
      if (json_parse_value(j, &val) < 0) return -1;
      if (lst_push(&arr->values, struct json_value, val) < 0) return -1;
      if (json_peek_ch(j) != ',') break;
      json_next_ch(j);
    }
    if (json_peek_ch(j) != ']') return -1;
  }
  json_next_ch(j);

  return 0;
}
int json_parse_value(struct json *j, struct json_value *val) {
  if (json_parse_whitespace(j) < 0) return -1;

  switch (json_peek_ch(j)) {
  case '"': { // string
    val->type   = JSON_STRING;
    val->string = json_parse_string(j);
    if (!val->string) return -1;
  } break;
  case '-':
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9': { // number
    val->type = JSON_NUMBER;
    if (json_parse_number(j, &val->number) < 0) return -1;
  } break;
  case '{': { // object
    val->type   = JSON_OBJECT;
    val->object = malloc(sizeof(struct json_object));
    if (json_parse_object(j, val->object) < 0) return -1;
  } break;
  case '[': { // array
    val->type  = JSON_ARRAY;
    val->array = malloc(sizeof(struct json_array));
    if (json_parse_array(j, val->array) < 0) return -1;
  } break;
  case 't':
  case 'f': { // bool 
    val->type = JSON_BOOL;
    if (json_parse_bool(j, &val->bool) < 0) return -1;
  } break;
  case 'n': { // null 
    val->type = JSON_NULL;
    json_next_ch(j);
    if (json_peek_ch(j) != 'u') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'l') return -1;
    json_next_ch(j);
    if (json_peek_ch(j) != 'l') return -1;
    json_next_ch(j);
  } break;
  }

  if (json_parse_whitespace(j) < 0) return -1;

  return 0;
}
int json_parse(struct json *j) {
  j->la = -1;
  if (lst_init(&j->objects, struct json_object, 32) < 0) return -1;

  while (json_peek_ch(j) != '\0') {
    if (json_parse_whitespace(j) < 0) return -1;

    struct json_object obj;
    if (json_parse_object(j, &obj) < 0) return -1;

    if (json_parse_whitespace(j) < 0) return -1;

    if (lst_push(&j->objects, struct json_object, obj) < 0) return -1;
  }

  return 0;
}
